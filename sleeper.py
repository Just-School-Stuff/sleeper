import os
import sys
from time import sleep
from datetime import timedelta, datetime

# import pdb    # For Debug
#    pdb.set_trace()

#################################################################################################################################
# Copyright (c) 2019 MUHAMMED SAID BILGEHAN
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. All advertising materials mentioning features or use of this software
#    must take permission from MUHAMMED SAID BILGEHAN and must display the
#	   following acknowledgement:
#    This product includes software developed by the MUHAMMED SAID BILGEHAN.
# 4. Neither the name of the MUHAMMED SAID BILGEHAN nor the
#    names of its contributors may be used to endorse or promote products
#    derived from this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY MUHAMMED SAID BILGEHAN ''AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL MUHAMMED SAID BILGEHAN BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#################################################################################################################################


# ===========================================================================================================================

notify = False  # Notification True-False
timeC = 0       # Time Counter
notHelp = 1     # is Help Parameter called
isApp = True    # is Application called? For GUI
OS = "P"        # Which OS running currently? For sleep and notify commands(Default is "P")

notifier = "ToastNotifier()"    # After install necessary Dependencies, it will be notifier = ToastNotifier()
ico = "E:\\WorkSpace\\python\\!BenimProgramlarım\\v3-LW\\uykucu.ico"

# ===========================================================================================================================


def DependCheck():
    global notifier
    OSCheck()
    if(OS == "W"):
        try:
            from win10toast import ToastNotifier
            notifier = ToastNotifier()
        except NameError:
            print("Some Dependencies need to installed before program starts: \n\twin10toast")
            ins = input("Do you want to continue wit install process (1 for yes, 0 for no):")

            if(ins == 1):
                os.system("pip install win10toast")
                notifier = ToastNotifier()
            else:
                End(1)
            # With this modification, program will install (if not installed) win10toast to send Notifications on Windows.

# ===========================================================================================================================


def OSCheck():
    global OS
    if(os.name == "nt"):
        OS = "W"    # Windows
    else:
        OS = "P"    # Posix or Posix Like (Linux etc...)

# ===========================================================================================================================


def Help():
    print("\tpython3 uykucu.py <TimeInSeconds> <Notifications> \n\tTimeInSeconds -> 300 (for 5 min delay)\n\tNotifications -> 1 for On, 0 for Off")
    return 0


# ===========================================================================================================================


def ArgCheck():
    global timeC
    global notify
    global notHelp

    for arg in sys.argv:
        if (arg == "--help") or (str(arg) == "-h"):
            notHelp = Help()
        if (arg.find):
            isApp = True

    if notHelp:
        if(len(sys.argv) > 1):
            timeC = int(sys.argv[1])
            notify = bool(sys.argv[2])
            return 0
        else:
            return 1


# ===========================================================================================================================


def Time():
    global timeC
    try:
        print("\tTimeTable:\n\t300 -> 5 min\n\t1200 -> 20 min\n\t3600 -> 60 min (1 hour)\n\tExample input for time '300' for 5 min")
        timeC = int(input("\tWait Time(in sec): "))
    except ValueError as ve:
        print("\n\t" + str(ve))
        print("Please try again...\n\n")
        Time()
    except:
        print("\n\tUnexpected Time() Error: \n\t")
        End(1)


# ===========================================================================================================================


def Notify():
    global notify
    try:
        notify = bool(int(input("\tNotify? (1 for Yes, 0 for No): ")))
    except ValueError as ve:
        print("\n\t" + str(ve))
        print("All values but 1 or 0 is unacceptable! Please try again...\n\n")
        Notify()
    except:
        print("\n\tUnexpected Notify() Error: \n\t")
        End(1)


# ===========================================================================================================================

# Notify part is made for specificity DeepinOS and after this version, I will change this part as universal
def Notifications(timeC):
    #    pdb.set_trace()

    global notifier, ico
    try:
        # more than 60 min (1 hour)  # After every 30 min, notification will be sent
        if (timeC >= 3600) and (int(timeC % 1800) == 0):
            if(OS == "P"):
                os.system("notify-send -c System -a deepin-system-monitor Uykucu \"Remaining Time - %s hour(s) \"" %
                          (format(timeC/3600, ".1f")))
            else:
                notifier.show_toast("Uykucu", "Remaining Time - %s hour(s)" %
                                    (format(timeC/3600, ".1f")), ico, 5)

            print("\n\tRemaining Time: %s hour(s)" % (format(timeC/3600, ".1f")))
        elif (timeC == 1200):    # less than 40 min  # Notification will sent at last 20 min
            if(OS == "P"):
                os.system(
                    "notify-send -c System -a deepin-system-monitor Uykucu \"Remaining Time - 20 minutes\"")
            else:
                notifier.show_toast("Uykucu", "Remaining Time - 20 minutes", ico, 5)

            print("\n\tRemaining Time: 20 minutes")

        elif (timeC == 300):    # less than 40 min  # Notification will sent at last 5 min
            if(OS == "P"):
                os.system(
                    "notify-send -c System -a deepin-system-monitor Uykucu \"Remaining Time - 5 minutes\"")
            else:
                notifier.show_toast("Uykucu", "Remaining Time - 5 minutes", ico, 5)

            print("\n\tRemaining Time: 5 minutes")

        elif (timeC == 15):     # less than 5 min   # Notification will sent at last 15 sec
            if(OS == "P"):
                os.system(
                    "notify-send -c System -a deepin-system-monitor Uykucu \"Remaining Time - 15 second\"")
            else:
                notifier.show_toast("Uykucu", "Remaining Time - 15 seconds", ico, 5)

            print("\n\tRemaining Time: 15 seconds")

        elif (timeC == 5):
            if(OS == "P"):
                os.system(
                    "notify-send -c System -a deepin-system-monitor Uykucu \"Remaining Time - Last 5 seconds\"")
            else:
                notifier.show_toast("Uykucu", "Remaining Time - Last 5 seconds", ico, 3)

            print("\n\tRemaining Time: 5 seconds")

    except:
        print("\n\tUnexpected Notification() Error: \n\t")
        End(1)


# ===========================================================================================================================


def BackCount():
    global timeC, OS
    print("System will be Suspended after " + str(timeC) + " seconds")

    print("Current Time: " + str(datetime.now()).split(' ')[1].split('.')[0])
    print("Suspend Time: " + str(datetime.now() +
                                 timedelta(seconds=timeC)).split(' ')[1].split('.')[0])

#    pdb.set_trace()

    while (timeC > 0):

        sleep(1)    # sleep 1 sec
        timeC -= 1  # Counter - 1 sec

        if (notify):
            Notifications(timeC)

    if(OS == "W"):
        os.system("psshutdown -d -t 0 -accepteula")  # Suspend the system on nt (windows)
    else:
        os.system("systemctl suspend")  # Suspend the system on Posix (linux etc...)

# ===========================================================================================================================


def End(r):
    exit(r)


# ===========================================================================================================================


def Main():
    global notHelp

    DependCheck()       # On Current version of Uykucu, new dependencies added for support cross-platform(Actually only for Windows)

    if(ArgCheck()):
        if (notHelp):   # if help parameter not called, than take time and notify information from user
            Time()      # Take time from user
            Notify()    # Take notify info from user

    if (notHelp):       # if help parameter not called, start counting time for suspend
        BackCount()


# ===========================================================================================================================


Main()
End(0)


# ===========================================================================================================================
